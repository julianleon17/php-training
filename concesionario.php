<?php

include('./funtions.php');

/***************
 * Objetos y/o Arrays
 ***************/

class carro {
    public $marca;
    public $visible;
    public $costo;
}

//Carro 1
$carro1 = new carro();
$carro1 -> marca = '(ford)';
$carro1 -> visible = true;
$carro1 -> costo = 12;

//Carro 2
$carro2 = new carro();
$carro2 -> marca = '(Nissan)';
$carro2 -> visible = true; 
$carro2 -> costo = 12;

//Carro 3
$carro3 = new carro();
$carro3 -> marca = '(Chevrolet-2020)';
$carro3 -> visible = true; 
$carro3 -> costo = 22;

//Carro 4
$carro4 = new carro();
$carro4 -> marca = '(Mazda 1)';
$carro4 -> visible = true; 
$carro4 -> costo = 16;

//Carro 5
$carro5 = new carro();
$carro5 -> marca = '(Mazda 2)';
$carro5 -> visible = true; 
$carro5 -> costo = 26;

//Carro 6
$carro6 = new carro();
$carro6 -> marca = '(Mazda 3)';
$carro6 -> visible = true; 
$carro6 -> costo = 36;

//Carro 7
$carro7 = new carro();
$carro7 -> marca = '(Kia-2019)';
$carro7 -> visible = true; 
$carro7 -> costo = 13;

//Carro 8
$carro8 = new carro();
$carro8 -> marca = '(Kia-2021)';
$carro8 -> visible = true; 
$carro8 -> costo = 23;

$carros = array(
    $carro1,
    $carro2,
    $carro3,
    $carro4,
    $carro5,
    $carro6,
    $carro7,
    $carro8      
);

$nombre = 'pedro';
$edad = 17;
$altura = 1.70;

$usuario = "$nombre $edad $altura";

$datos = explode(",",$usuario);

echo $carros[0]->marca;



























//prueba();
//var_dump($carros);die;


/**************
 * Ejecuciones
 **************/

//echo "
//    *****************\n
//    Bienvenido :D!\n
//    *****************\n
//    \n";

//menu();  


?>
